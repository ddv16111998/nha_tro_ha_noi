<!doctype html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang=""> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8" lang=""> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9" lang=""> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang=""> <!--<![endif]-->

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <title>DDV - Admin</title>
    <link rel="icon" type="image/ico" href="{{asset('assets/images/favicon.ico')}}" />
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">


    <!-- ============================================
    ================= Stylesheets ===================
    ============================================= -->
    <!-- vendor css files -->
    <link rel="stylesheet" href="{{asset('assets/css/vendor/bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{asset('assets/css/vendor/animate.css')}}">
    <link rel="stylesheet" href="{{asset('assets/css/vendor/font-awesome.min.css')}}">
    <link rel="stylesheet" href="{{asset('assets/js/vendor/animsition/css/animsition.min.css')}}">

    <!-- project main css files -->
    <link rel="stylesheet" href="{{asset('assets/css/main.css')}}">
    <!--/ stylesheets -->



    <!-- ==========================================
    ================= Modernizr ===================
    =========================================== -->
    <script src="{{asset('assets/js/vendor/modernizr/modernizr-2.8.3-respond-1.4.2.min.js')}}"></script>
    <!--/ modernizr -->


</head>


<body id="minovate" class="appWrapper">

<!-- ====================================================
================= Application Content ===================
===================================================== -->
@yield('content')
<!--/ Application Content -->


<!-- ============================================
============== Vendor JavaScripts ===============
============================================= -->
<script src="ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
<script>window.jQuery || document.write('<script src="{{asset('assets/js/vendor/jquery/jquery-1.11.2.min.js')}}"><\/script>')</script>

<script src="{{asset('assets/js/vendor/bootstrap/bootstrap.min.js')}}"></script>

<script src="{{asset('assets/js/vendor/jRespond/jRespond.min.js')}}"></script>

<script src="{{asset('assets/js/vendor/sparkline/jquery.sparkline.min.js')}}"></script>

<script src="{{asset('assets/js/vendor/slimscroll/jquery.slimscroll.min.js')}}"></script>

<script src="{{asset('assets/js/vendor/animsition/js/jquery.animsition.min.js')}}"></script>

<script src="{{asset('assets/js/vendor/screenfull/screenfull.min.js')}}"></script>
<!--/ vendor javascripts -->




<!-- ============================================
============== Custom JavaScripts ===============
============================================= -->
<script src="{{asset('assets/js/main.js')}}"></script>
<!--/ custom javascripts -->


<!-- ===============================================
============== Page Specific Scripts ===============
================================================ -->
<!--/ Page Specific Scripts -->



</body>
</html>
