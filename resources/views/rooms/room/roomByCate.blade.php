@extends('rooms.master')
@section('css')
    <style>
        .animation.masonry-item{
            height: 419px;
        }
        .property-thumb-info-content{
            height: 200px;
        }
    </style>
@stop
@section('js')
    <script>
        $.ajaxSetup({headers:{'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')}});
        function Filter() {
            order = $('#order-status').val();
            sort = $('#sortby-status').val();
            // alert(sort);
            var formData = new FormData();
            formData.append('order',order);
            formData.append('sort',sort);
            $.ajax({
                type:"POST",
                url:"{{route('room.byCateAndFilter',['cateId'=>$cateId])}}",
                data:formData,
                dataType:'text',
                contentType: false,
                processData: false,
                success:function (data) {
                    $('.masonry-desk').html(data);
                }
            })
        }
    </script>
@stop
@section('content')
    <!-- Begin Main -->
    <div role="main" class="main pgl-bg-grey">
        <section class="pgl-properties pgl-bg-grey">
            <div class="container">
                <h2>Nhà / Phòng theo danh mục</h2>
                <div class="properties-full">
                    <div class="listing-header clearfix">
                        <ul class="list-inline list-icons pull-left">
                            <li class="active"><a href="{{route('room.byCate',$cateId)}}"><i class="fa fa-th"></i></a></li>
                            <li><a href="{{route('room.byCateFullWidth',$cateId)}}"><i class="fa fa-th-list"></i></a></li>
                            <li><a href="list-map.html"><i class="fa fa-map-marker"></i></a></li>
                        </ul>
                        <ul class="list-inline list-sort pull-right">
                            <li><label for="order-status">Order</label></li>
                            <li>
                                <select id="order-status" name="order-status" data-placeholder="Order" class="chosen-select" onchange="Filter()">
                                    <option value="desc">Giảm dần</option>
                                    <option value="asc">Tăng dần</option>
                                </select>
                            </li>
                            <li><label for="sortby-status">Sort by</label></li>
                            <li>
                                <select id="sortby-status" name="sortby-status" data-placeholder="Sort by" class="chosen-select" onchange="Filter()">
                                    <option value="created_at">Date</option>
                                    <option value="acreage">Diện tích</option>
                                    <option value="title">Tên</option>
                                </select>
                            </li>
                        </ul>
                    </div>
                    <div class="row">
                        <div class="masonry-desk">
                            @foreach($rooms as $room)
                                <div class="col-xs-6 col-md-3 animation masonry-item">
                                    <div class="pgl-property">
                                        <div class="property-thumb-info">
                                            <div class="property-thumb-info-image">
                                                <img alt="" class="" src="{{ URL::to('/rooms/images/rooms') }}/{{ json_decode($room->images)[0] }}" height="150px" width="100%">
                                                <span class="property-thumb-info-label">
    													<span class="label price">{{number_format($room->price)}}</span>
    													<span class="label forrent">Rent</span>
    												</span>
                                            </div>
                                            <div class="property-thumb-info-content">
                                                <h3><a href="{{route('room.details',$room->id)}}"><b>{{$room->title}}</b></a></h3>
                                                <address><b>Địa điểm:</b> {{$room->address}}</address>
                                                <p><b>Thời gian đăng:</b> {{$room->created_at}}</p>
                                            </div>
                                            <div class="amenities clearfix">
                                                <ul class="pull-left">
                                                    <li><strong>Diện tích:</strong> {{$room->acreage}}<sup>m2</sup></li>
                                                </ul>
                                                <ul class="pull-right">
                                                    <li><i class="fa fa-eye"></i> Lượt xem: {{$room->count_views}}</li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                        {!! $rooms->links() !!}
                    </div>

                </div>
            </div>
        </section>
        <!-- End Properties -->

    </div>
    <!-- End Main -->
@stop
