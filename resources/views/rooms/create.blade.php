@extends('rooms.masterInOut')
@section('content')
    <div id="wrap" class="animsition">


        <div class="page page-core page-login">

            <div class="text-center"><h3 class="text-light text-white"><span class="text-lightred">D</span>DV</h3></div>

            <div class="container w-420 p-15 bg-white mt-40 text-center">


                <h2 class="text-light text-greensea">Đăng Ký</h2>
                <form class="form-validation mt-20" action="{{route('admin.user.store')}}" method="POST">
                    @csrf
                    <div class="form-group">
                        <input type="username" class="form-control underline-input" placeholder="Tài khoản" name="username" value="{{old('username')}}">
                    </div>
                    @error('username')
                    <span class="invalid-feedback alert-danger" role="alert">
                        <p style="font-style: italic">{{ $message }}</p>
                    </span>
                    @enderror

                    <div class="form-group">
                        <input type="name" class="form-control underline-input" placeholder="Họ tên" name="hoten" value="{{old('hoten')}}">
                    </div>
                    @error('hoten')
                    <span class="invalid-feedback alert-danger" role="alert">
                        <p style="font-style: italic">{{ $message }}</p>
                    </span>
                    @enderror

                    <div class="form-group">
                        <input type="email" class="form-control underline-input" placeholder="Email" name="email" value="{{old('email')}}">
                    </div>
                    @error('email')
                    <span class="invalid-feedback alert-danger" role="alert">
                        <p style="font-style: italic">{{ $message }}</p>
                    </span>
                    @enderror

                    <div class="form-group">
                        <input type="password" placeholder="Mật khẩu" class="form-control underline-input" name="password">
                    </div>
                    @error('password')
                    <span class="invalid-feedback alert-danger" role="alert">
                        <p style="font-style: italic">{{ $message }}</p>
                    </span>
                    @enderror
                    <div class="form-group">
                        <input type="password" placeholder="Nhập lại mật khẩu" class="form-control underline-input" name="password_verified">
                    </div>
                    @error('password_verified')
                    <span class="invalid-feedback alert-danger" role="alert">
                        <p style="font-style: italic">{{ $message }}</p>
                    </span>
                    @enderror
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group text-left mt-20">
                                <button type="submit" class="btn btn-greensea b-0 br-2 mr-5">Đăng ký</button>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group text-right mt-20">
                                <a href="" class="btn btn-primary">Quay lại</a>
                            </div>
                        </div>
                    </div>



                </form>

                <hr class="b-3x">

                <div class="social-login text-left">

                    <ul class="pull-right list-unstyled list-inline">
                        <li class="p-0">
                            <a class="btn btn-sm btn-primary b-0 btn-rounded-20" href="javascript:;"><i class="fa fa-facebook"></i></a>
                        </li>
                        <li class="p-0">
                            <a class="btn btn-sm btn-info b-0 btn-rounded-20" href="javascript:;"><i class="fa fa-twitter"></i></a>
                        </li>
                        <li class="p-0">
                            <a class="btn btn-sm btn-lightred b-0 btn-rounded-20" href="javascript:;"><i class="fa fa-google-plus"></i></a>
                        </li>
                        <li class="p-0">
                            <a class="btn btn-sm btn-primary b-0 btn-rounded-20" href="javascript:;"><i class="fa fa-linkedin"></i></a>
                        </li>
                    </ul>

                    <h5>Or login with</h5>

                </div>


            </div>

        </div>



    </div>
@stop
