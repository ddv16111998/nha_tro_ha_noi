<!DOCTYPE html>
<html lang="en">

<!-- Mirrored from pixelgeeklab.com/html/realestast/grid-masonry-4-column.html by HTTrack Website Copier/3.x [XR&CO'2014], Sun, 16 Aug 2015 08:54:40 GMT -->
<head>
    <meta charset="utf-8">
    <meta name="keywords" content="HTML5 Template" />
    <meta name="description" content="Flatize - Shop HTML5 Responsive Template">
    <meta name="author" content="pixelgeeklab.com">
    <base href="{{asset('')}}">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Nhà trọ Hà Nội</title>
    {{--    css--}}
    <link rel="stylesheet" href="{{asset('rooms/css/style.css')}}">
    <!-- Google Fonts -->
    <link href='http://fonts.googleapis.com/css?family=Rochester' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Raleway:400,700' rel='stylesheet' type='text/css'>

    <!-- Bootstrap -->
    <link href="{{asset('rooms/bootstrap/css/bootstrap.min.css')}}" rel="stylesheet">

    <!-- Libs CSS -->
    <link href="{{asset('rooms/css/fonts/font-awesome/css/font-awesome.css')}}" rel="stylesheet">
    <link rel="stylesheet" href="{{asset('rooms/vendor/owl-carousel/owl.carousel.css')}}" media="screen">
    <link rel="stylesheet" href="{{asset('rooms/vendor/owl-carousel/owl.theme.css')}}" media="screen">
    <link rel="stylesheet" href="{{asset('rooms/vendor/flexslider/flexslider.css')}}" media="screen">
    <link rel="stylesheet" href="{{asset('rooms/vendor/chosen/chosen.css')}}" media="screen">

    <!-- Theme -->
    <link href="{{asset('rooms/css/theme-animate.css')}}" rel="stylesheet">
    <link href="{{asset('rooms/css/theme-elements.css')}}" rel="stylesheet">
    <link href="{{asset('rooms/css/theme-blog.css')}}" rel="stylesheet">
    <link href="{{asset('rooms/css/theme-map.css')}}" rel="stylesheet">
    <link href="{{asset('rooms/css/theme.css')}}" rel="stylesheet">

    <!--File Input-->
    <link rel="stylesheet" href="{{asset('rooms/file-input/fileinput.css')}}">
    <!-- Style Switcher-->
    <link rel="stylesheet" href="{{asset('rooms/style-switcher/css/style-switcher.css')}}">
{{--    <link href="{{asset('rooms/css/colors/red/style.html')}}" rel="stylesheet" id="layoutstyle">--}}

    <!-- Theme Responsive-->
    <link href="{{asset('rooms/css/theme-responsive.css')}}" rel="stylesheet">
    @yield('css')
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>

<!-- JS comment facebook-->
<div id="fb-root"></div>
<script async defer crossorigin="anonymous" src="https://connect.facebook.net/vi_VN/sdk.js#xfbml=1&version=v4.0&appId=448606319342803&autoLogAppEvents=1"></script>
<!-- end JS comment fb -->

<div id="page">
@include('rooms.layouts.header')
@yield('content')
@include('rooms.layouts.footer')
@include('rooms.layouts.switcher')
</div>

<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="{{asset('rooms/vendor/jquery.min.js')}}"></script>
<!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="{{asset('rooms/bootstrap/js/bootstrap.min.js')}}"></script>
<script src="{{asset('rooms/vendor/owl-carousel/owl.carousel.js')}}"></script>
<script src="{{asset('rooms/vendor/flexslider/jquery.flexslider-min.js')}}"></script>
<script src="{{asset('rooms/vendor/chosen/chosen.jquery.min.js')}}"></script>
<script src="{{asset('rooms/vendor/masonry/imagesloaded.pkgd.min.js')}}"></script>
<script src="{{asset('rooms/vendor/masonry/masonry.pkgd.min.js')}}"></script>
{{--<!-- JS Dropzone -->--}}
{{--<script src="{{asset('rooms/dropzone/dropzone.js')}}"></script>--}}
<!--JS File Input-->
<script src="{{asset('rooms/file-input/js/fileinput.js')}}"></script>
<script src="{{asset('rooms/file-input/js/vi.js')}}"></script>
<!-- Theme Initializer -->
<script src="{{asset('rooms/js/theme.plugins.js')}}"></script>
<script src="{{asset('rooms/js/theme.js')}}"></script>

<!-- Style Switcher -->
<script type="text/javascript" src="{{asset('rooms/style-switcher/js/switcher.js')}}"></script>

<!-- ckeditor JS -->
<script src="{{asset('rooms/ckeditor/ckeditor.js')}}"></script>
<script> CKEDITOR.replace('editor1'); </script>

<script>
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': '{{ csrf_token() }}',
        }
    });

    function get_location(local) {
        var district_id = $(local).val();
        // console.log(district_id);
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': '{{ csrf_token() }}',
            }
        });
        if($(local).attr('id') == 'district_id')
        {
            $.ajax({
                url:"{{route('get_location')}}",
                dataType:"html",
                data: {district_id:district_id}
            })
                .done(function (data) {
                    $('#commune_id').html(data);
                })
        }
        if($(local).attr('id') == 'header_district_id')
        {
            $.ajax({
                url:"{{route('get_location')}}",
                dataType:"html",
                data: {district_id:district_id}
            })
                .done(function (data) {
                    $('#header_commune_id').html(data);
                })
        }

    }
    $('#btn-search').click(function () {
        $('.iconload').show();
        var header_district_id = $('#header_district_id').val();
        var header_commune_id = $('#header_commune_id').val();
        var cate_id = $('#cate_id').val();
        var minprice = $('#minprice').val();
        var maxprice = $('#maxprice').val();
        var formData = new FormData();
        formData.append('district_id',header_district_id);
        formData.append('commune_id',header_commune_id);
        formData.append('cate_id',cate_id);
        formData.append('minprice',minprice);
        formData.append('maxprice',maxprice);
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': '{{ csrf_token() }}',
            }
        });
        $.ajax({
            type : "POST",
            url :"{{route('room.search')}}",
            data : formData,
            dataType: 'html',
            contentType: false,
            processData: false,
            success: function (data) {
                $('.iconload').hide();
                $('.result').html(data);
            }
        })
    })

</script>
@yield('js')
</body>
</html>
