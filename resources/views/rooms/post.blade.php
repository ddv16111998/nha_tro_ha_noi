@extends('rooms.master')
@section('css')
    <style>
        /*#map {*/
        /*    width: 100%;*/
        /*    height: 400px;*/
        /*    background-color: grey;*/
        /*}*/
    </style>
@stop
@section('js')
    <script type="text/javascript">
        $('#file-image').fileinput({
            theme: 'fa',
            language: 'vi',
            showUpload: false,
            allowedFileExtensions: ['jpg', 'png', 'gif']
        });
    </script>
    <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCzlVX517mZWArHv4Dt3_JVG0aPmbSE5mE&callback=initialize&libraries=geometry,places" async defer></script>
    <script>
        var map;
        var marker;
        function initialize() {
            var mapOptions = {
                center: {lat: 21.0227788, lng: 105.8194112},
                zoom: 12
            };
            map = new google.maps.Map(document.getElementById('map-canvas'),
                mapOptions);


            // Get GEOLOCATION
            if (navigator.geolocation) {
                navigator.geolocation.getCurrentPosition(function(position) {
                    var pos = new google.maps.LatLng(position.coords.latitude,
                        position.coords.longitude);
                    var geocoder = new google.maps.Geocoder();
                    geocoder.geocode({
                        'latLng': pos
                    }, function (results, status) {
                        if (status ==
                            google.maps.GeocoderStatus.OK) {
                            if (results[0]) {
                                console.log(results[0].formatted_address);
                            } else {
                                console.log('No results found');
                            }
                        } else {
                            console.log('Geocoder failed due to: ' + status);
                        }
                    });
                    map.setCenter(pos);
                    marker = new google.maps.Marker({
                        position: pos,
                        map: map,
                        draggable: true
                    });
                }, function() {
                    handleNoGeolocation(true);
                });
            } else {
                // Browser doesn't support Geolocation
                handleNoGeolocation(false);
            }

            function handleNoGeolocation(errorFlag) {
                if (errorFlag) {
                    var content = 'Error: The Geolocation service failed.';
                } else {
                    var content = 'Error: Your browser doesn\'t support geolocation.';
                }

                var options = {
                    map: map,
                    zoom: 19,
                    position: new google.maps.LatLng(21.0227788,105.8194112),
                    content: content
                };

                map.setCenter(options.position);
                marker = new google.maps.Marker({
                    position: options.position,
                    map: map,
                    zoom: 19,
                    icon: "rooms/images/rooms/gps.png",
                    draggable: true
                });
                /* Dragend Marker */
                google.maps.event.addListener(marker, 'dragend', function() {
                    var geocoder = new google.maps.Geocoder();
                    geocoder.geocode({'latLng': marker.getPosition()}, function(results, status) {
                        if (status == google.maps.GeocoderStatus.OK) {
                            if (results[0]) {
                                $('#location-text-box').val(results[0].formatted_address);
                                $('#txtaddress').val(results[0].formatted_address);
                                $('#txtlat').val(marker.getPosition().lat());
                                $('#txtlng').val(marker.getPosition().lng());
                                infowindow.setContent(results[0].formatted_address);
                                infowindow.open(map, marker);
                            }
                        }
                    });
                });
                /* End Dragend */

            }

            // get places auto-complete when user type in location-text-box
            var input = /** @type {HTMLInputElement} */
                (
                    document.getElementById('location-text-box'));


            var autocomplete = new google.maps.places.Autocomplete(input);
            autocomplete.bindTo('bounds', map);

            var infowindow = new google.maps.InfoWindow();
            marker = new google.maps.Marker({
                map: map,
                icon: "rooms/images/rooms/gps.png",
                anchorPoint: new google.maps.Point(0, -29),
                draggable: true
            });

            google.maps.event.addListener(autocomplete, 'place_changed', function() {
                infowindow.close();
                marker.setVisible(false);
                var place = autocomplete.getPlace();
                if (!place.geometry) {
                    return;
                }
                var geocoder = new google.maps.Geocoder();
                geocoder.geocode({'latLng': place.geometry.location}, function(results, status) {
                    if (status == google.maps.GeocoderStatus.OK) {
                        if (results[0]) {
                            $('#txtaddress').val(results[0].formatted_address);
                            infowindow.setContent(results[0].formatted_address);
                            infowindow.open(map, marker);
                        }
                    }
                });
                // If the place has a geometry, then present it on a map.
                if (place.geometry.viewport) {
                    map.fitBounds(place.geometry.viewport);
                } else {
                    map.setCenter(place.geometry.location);
                    map.setZoom(17); // Why 17? Because it looks good.
                }
                marker.setIcon( /** @type {google.maps.Icon} */ ({
                    url: "rooms/images/rooms/gps.png"
                }));
                document.getElementById('txtlat').value = place.geometry.location.lat();
                document.getElementById('txtlng').value = place.geometry.location.lng();
                console.log(place.geometry.location.lat());
                marker.setPosition(place.geometry.location);
                marker.setVisible(true);

                var address = '';
                if (place.address_components) {
                    address = [
                        (place.address_components[0] && place.address_components[0].short_name || ''), (place.address_components[1] && place.address_components[1].short_name || ''), (place.address_components[2] && place.address_components[2].short_name || '')
                    ].join(' ');
                }
                /* Dragend Marker */
                google.maps.event.addListener(marker, 'dragend', function() {
                    var geocoder = new google.maps.Geocoder();
                    geocoder.geocode({'latLng': marker.getPosition()}, function(results, status) {
                        if (status == google.maps.GeocoderStatus.OK) {
                            if (results[0]) {
                                $('#location-text-box').val(results[0].formatted_address);
                                $('#txtlat').val(marker.getPosition().lat());
                                $('#txtlng').val(marker.getPosition().lng());
                                infowindow.setContent(results[0].formatted_address);
                                infowindow.open(map, marker);
                            }
                        }
                    });
                });
                /* End Dragend */
            });


        }
    </script>
@stop
@section('content')
    <div role="main" class="main pgl-bg-grey">
        <section class="pgl-advanced-post pgl-bg-light" style="padding-top: 100px">
            <div class="container">
                <div class="content-header">
                    <h1>Đăng tin phòng</h1>
                </div>
                <div class="content-body mt-4">
                    <form action="{{route('room.store')}}" method="POST" enctype="multipart/form-data">
                        @csrf
                        <div class="form-group">
                            <label for="title">Tiêu đề</label>
                            <input type="text" class="form-control" name="title" placeholder="Nhập tiêu đề">
                        </div>
                        <div class="form-group">
                            <label for="price">Giá</label>
                            <input type="text" class="form-control" name="price" placeholder="Nhập giá">
                        </div>
                        <div class="form-group">
                            <label for="acreage">Diện tích phòng</label>
                            <input type="text" class="form-control" name="acreage" placeholder="Nhập diện tích m2">
                        </div>
                        <div class="form-group">
                            <label for="district_id">Tỉnh / Thành phố</label>
                            <select name="district_id" id="district_id" class="form-control" onchange="get_location($(this))">
                                <option value=""> -- Chọn tỉnh / thành phố --</option>
                                @foreach($districts as $district)
                                    <option value="{{$district->id}}">{{$district->name}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="commune_id">Quận / Huyện</label>
                            <select name="commune_id" id="commune_id" class="form-control">
                                <option value=""> -- Chọn quận / huyện --</option>
                            </select>
                        </div>
{{--                        <div class="form-group">--}}
{{--                            <label for="address">Địa chỉ</label>--}}
{{--                            <input type="text" class="form-control" name="address" placeholder="Nhập địa chỉ cụ thể">--}}
{{--                        </div>--}}
                        <div class="form-group">
                            <label>Địa chỉ phòng trọ:</label> Bạn có thể nhập hoặc chọn ví trí trên bản đồ
                            <input type="text" id="location-text-box" name="address" class="form-control" value="" autocomplete="off" />
                            <p><i class="far fa-bell"></i> Nếu địa chỉ hiển thị bên bản đồ không đúng bạn có thể điều chỉnh bằng cách kéo điểm màu xanh trên bản đồ tới vị trí chính xác.</p>
                            <input type="hidden" id="txtaddress" name="txtaddress" value=""  class="form-control"  />
                            <input type="hidden" id="txtlat" value="" name="txtlat"  class="form-control"  />
                            <input type="hidden" id="txtlng"  value="" name="txtlng" class="form-control" />
                        </div>
                        <div id="map-canvas" style="width: auto; height: 400px;"></div>
                        <div class="form-group">
                            <label for="cate_id">Danh mục</label>
                            <select name="cate_id" id="cate_id" class="form-control">
                                <option value=""> -- Chọn danh mục --</option>
                                @foreach($cates as $cate)
                                    <option value="{{$cate->id}}">{{$cate->name}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="phone">Số điện thoại liên hệ</label>
                            <input type="text" class="form-control" name="phone" placeholder="Nhập số điện thoại">
                        </div>
                        <div class="form-group">
                            <label for="">Chọn ảnh</label>
                            <div class="file-loading">
                                <input id="file-image" type="file" class="file" name="images[]" multiple data-preview-file-type="any" data-upload-url="#">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="">Ghi chú</label>
                            <textarea class="form-control" id="editor1" name="description"></textarea>
                        </div>
                        <div class="modal-footer">
                            <button type="submit" class="btn btn-primary">Lưu</button>
                            <a href="{{route('room.index')}}" class="btn btn-darkgray">Thoát</a>
                        </div>
                    </form>
                </div>
            </div>
        </section>
    </div>
@stop



