@extends('admin.master')
@section('css')
@stop
@section('js')
    <script>
        $(function() {
            $('#listData').DataTable({
                processing: true,
                serverSide: true,
                ajax: '{!! route('admin.user.index') !!}',
                columns: [
                    { data: 'id', name: 'id' },
                    { data: 'avatar', name: 'avatar' },
                    { data: 'username', name: 'username' },
                    { data: 'email', name: 'email' },
                    { data: 'role', name: 'role' },
                    { data : 'status', name:'status'},
                    { data: 'action', name: 'action' }
                ]
            });
        });
    </script>
@stop
@section('content')
    <section id="content">
        <div class="page page-index">

            <div class="pageheader">

                <h2>Danh sách người dùng <span>// You can place subtitle here</span></h2>

                <div class="page-bar">

                    <ul class="page-breadcrumb">
                        <li>
                            <a href="{{route('admin.dashboard.index')}}"><i class="fa fa-home"></i> DDV</a>
                        </li>
                        <li>
                            <a href="{{ route('admin.user.index') }}">Danh sách người dùng</a>
                        </li>
                    </ul>

                    <div class="page-toolbar">
                        <a role="button" tabindex="0" class="btn btn-lightred no-border pickDate">
                            <i class="fa fa-calendar"></i>&nbsp;&nbsp;<span></span>&nbsp;&nbsp;<i class="fa fa-angle-down"></i>
                        </a>
                    </div>

                </div>

            </div>
            <div class="container-fluid">
                @if(session('thongbao'))
                    <div class="alert bg-success">
                        <button type="button" class="close" data-dismiss="alert"><span>×</span><span class="sr-only">Close</span></button>
                        <span class="text-semibold">Well done!</span>  {{session('thongbao')}}
                    </div>
                @endif
                <div class="tile-body p-0">
                    <table class="table table-bordered" id="listData">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>Avatar</th>
                            <th>UserName</th>
                            <th>Email</th>
                            <th>Quyền</th>
                            <th>Trạng thái</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                    </table>

                </div>
            </div>

        </div>
    </section>
@stop


