<!doctype html>
<html class="no-js" lang="">
<head>

        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <title>DDV - Admin</title>
        <link rel="icon" type="image/ico" href="{{asset('assets/images/favicon.ico')}}" />
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <!-- ============================================
        ================= Stylesheets ===================
        ============================================= -->
        <!-- vendor css files -->
        <link rel="stylesheet" href="{{asset('assets/css/vendor/bootstrap.min.css')}}">
        <link rel="stylesheet" href="{{asset('assets/css/vendor/animate.css')}}">
        <link rel="stylesheet" href="{{asset('assets/css/vendor/font-awesome.min.css')}}">
        <link rel="stylesheet" href="{{asset('assets/js/vendor/animsition/css/animsition.min.css')}}">
        <link rel="stylesheet" href="{{asset('assets/js/vendor/daterangepicker/daterangepicker-bs3.css')}}">
        <link rel="stylesheet" href="{{asset('assets/js/vendor/morris/morris.css')}}">
        <link rel="stylesheet" href="{{asset('assets/js/vendor/owl-carousel/owl.carousel.css')}}">
        <link rel="stylesheet" href="{{asset('assets/js/vendor/owl-carousel/owl.theme.css')}}">
        <link rel="stylesheet" href="{{asset('assets/js/vendor/rickshaw/rickshaw.min.css')}}">
        <link rel="stylesheet" href="{{asset('assets/js/vendor/datetimepicker/css/bootstrap-datetimepicker.min.css')}}">
        <link rel="stylesheet" href="{{asset('assets/js/vendor/datatables/css/jquery.dataTables.min.css')}}">
        <link rel="stylesheet" href="{{asset('assets/js/vendor/datatables/datatables.bootstrap.min.css')}}">
        <link rel="stylesheet" href="{{asset('assets/js/vendor/chosen/chosen.css')}}">
        <link rel="stylesheet" href="{{asset('assets/js/vendor/summernote/summernote.css')}}">

        <!-- project main css files -->
        <link rel="stylesheet" href="{{asset('assets/css/main.css')}}">
        <!--/ stylesheets -->



        <!-- ==========================================
        ================= Modernizr ===================
        =========================================== -->
        <script src="{{asset('assets/js/vendor/modernizr/modernizr-2.8.3-respond-1.4.2.min.js')}}"></script>
        <!--/ modernizr -->
        @yield('css')
    </head>





    <body id="minovate" class="appWrapper">


        <!-- ====================================================
        ================= Application Content ===================
        ===================================================== -->
        <div id="wrap" class="animsition">
            @include('admin.layout.header')
            <!-- =================================================
            ================= CONTROLS Content ===================
            ================================================== -->
            <div id="controls">
                @include('admin.layout.sidebar')
                @include('admin.layout.rightbar')
                <!-- =================================================
                ================= RIGHTBAR Content ===================
                ================================================== -->

                <!--/ RIGHTBAR Content -->

            </div>
            <!--/ CONTROLS Content -->

            <!-- ====================================================
            ================= CONTENT ===============================
            ===================================================== -->
            @yield('content')
            <!--/ CONTENT -->






        </div>
        <!--/ Application Content -->

        <!-- ============================================
        ============== Vendor JavaScripts ===============
        ============================================= -->
        <script src="{{ asset('assets/ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js') }}"></script>
        <script>window.jQuery || document.write('<script src="{{asset('assets/js/vendor/jquery/jquery-1.11.2.min.js')}}"><\/script>')</script>

        <script src="{{asset('assets/js/vendor/bootstrap/bootstrap.min.js')}}"></script>

        <script src="{{asset('assets/js/vendor/jRespond/jRespond.min.js')}}"></script>

        <script src="{{asset('assets/js/vendor/d3/d3.min.js')}}"></script>
        <script src="{{asset('assets/js/vendor/d3/d3.layout.min.js')}}"></script>

        <script src="{{asset('assets/js/vendor/rickshaw/rickshaw.min.js')}}"></script>

        <script src="{{asset('assets/js/vendor/sparkline/jquery.sparkline.min.js')}}"></script>

        <script src="{{asset('assets/js/vendor/slimscroll/jquery.slimscroll.min.js')}}"></script>

        <script src="{{asset('assets/js/vendor/animsition/js/jquery.animsition.min.js')}}"></script>

        <script src="{{asset('assets/js/vendor/daterangepicker/moment.min.js')}}"></script>
        <script src="{{asset('assets/js/vendor/daterangepicker/daterangepicker.js')}}"></script>

        <script src="{{asset('assets/js/vendor/screenfull/screenfull.min.js')}}"></script>

        <script src="{{asset('assets/js/vendor/flot/jquery.flot.min.js')}}"></script>
        <script src="{{asset('assets/js/vendor/flot-tooltip/jquery.flot.tooltip.min.js')}}"></script>
        <script src="{{asset('assets/js/vendor/flot-spline/jquery.flot.spline.min.js')}}"></script>

        <script src="{{asset('assets/js/vendor/easypiechart/jquery.easypiechart.min.js')}}"></script>

        <script src="{{asset('assets/js/vendor/raphael/raphael-min.js')}}"></script>
        <script src="{{asset('assets/js/vendor/morris/morris.min.js')}}"></script>

        <script src="{{asset('assets/js/vendor/owl-carousel/owl.carousel.min.js')}}"></script>

        <script src="{{asset('assets/js/vendor/datetimepicker/js/bootstrap-datetimepicker.min.js')}}"></script>

        <script src="{{asset('assets/js/vendor/datatables/js/jquery.dataTables.min.js')}}"></script>
        <script src="{{asset('assets/js/vendor/datatables/extensions/dataTables.bootstrap.js')}}"></script>

        <script src="{{asset('assets/js/vendor/chosen/chosen.jquery.min.js')}}"></script>

        <script src="{{asset('assets/js/vendor/summernote/summernote.min.js')}}"></script>

        <script src="{{asset('assets/js/vendor/coolclock/coolclock.js')}}"></script>
        <script src="{{asset('assets/js/vendor/coolclock/excanvas.js')}}"></script>
        <!--/ vendor javascripts -->




        <!-- ============================================
        ============== Custom JavaScripts ===============
        ============================================= -->
        <script src="{{asset('assets/js/main.js')}}"></script>
        <!--/ custom javascripts -->
        @yield('js')

    </body>

<!-- Tieu Long Lanh Kute From Baobinh.net Free CSS HTML Responsive template download-->
</html>
