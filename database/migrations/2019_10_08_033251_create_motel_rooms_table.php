<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMotelRoomsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('motel_rooms', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('title');
            $table->string('address');
            $table->string('price');
            $table->string('acreage')->comment('diện tích');
            $table->unsignedBigInteger('district_id');
            $table->foreign('district_id')->references('id')->on('district');
            $table->unsignedBigInteger('cate_id');
            $table->foreign('cate_id')->references('id')->on('cate');
            $table->string('description')->nullable();
            $table->unsignedBigInteger('user_id');
            $table->foreign('user_id')->references('id')->on('users');
            $table->string('phone');
            $table->string('images');
            $table->integer('count_views')->default(0);
            $table->integer('approve')->default(0)->comment('phê duyệt');
            $table->timestamps();
            $table->timestamp('deleted_at')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('motel_rooms');
    }
}
