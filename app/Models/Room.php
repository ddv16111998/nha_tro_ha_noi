<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Room extends Model
{
    use SoftDeletes;
    protected $table = 'motel_rooms';
    protected $guarded = [];
    protected $dates = ['deleted_at'];

    public function category()
    {
        return $this->belongsTo('App\Models\Cate','cate_id');
    }
    public function getUser()
    {
        return $this->belongsTo('App\Models\User','user_id');
    }
    public function comment()
    {
        return $this->hasMany('App\Models\Comments','post_id');
    }
}
