<?php
namespace App\Repositories;
use App\Models\User;
class AdminRepository
{
    public function getDataIndex()
    {
        $data = User::whereNull('deleted_at')->where(['role'=>0])->get();
        return $data;
    }
    public function getDataById($id)
    {
        $data = User::find($id);
        return $data;
    }
    public function updateOrCreate($data,$id)
    {
        $data = User::updateOrCreate(['id'=>$id],$data);
        if($data)
        {
            return true;
        }
        return false;
    }
    public function destroy($id)
    {
        $data = User::find($id);
        $data->delete();
        if($data->trashed())
        {
            return true;
        }
        return false;
    }
}
