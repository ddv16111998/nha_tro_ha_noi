<?php
namespace App\Repositories;
use App\Models\Cate;
class CateRepository
{
    public function getDataIndex()
    {
        $data = Cate::whereNull('deleted_at')->get();
        return $data;
    }
    public function getDataById($id)
    {
        $data = Cate::find($id);
        return $data;
    }
    public function updateOrCreate($data,$id)
    {
        $cate = Cate::updateOrCreate(['id'=>$id],$data);
        if($cate)
        {
            return true;
        }
        return false;
    }
    public function destroy($id)
    {
        $data = Cate::find($id);
        $data->delete();
        if($data->trashed())
        {
            return true;
        }
        return false;
    }
}
