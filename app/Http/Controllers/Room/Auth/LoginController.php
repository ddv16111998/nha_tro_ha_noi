<?php

namespace App\Http\Controllers\Room\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Validation\ValidationException;
use App\Models\User;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use App\Http\Requests\StoreLoginPost;
class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = "/";

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    public function guard()
    {
        return Auth::guard('web');
    }

    public function showLoginForm()
    {
        return view('rooms.auth.login');
    }
    public function handleLogin(Request $request)
    {
        $data = $this->guard()->attempt(['email'=>$request->email, 'password'=>$request->password, 'status'=>1,'role'=>1]);
        if($data)
        {
            return redirect()->route('room.index');
        }
        else{
            $request->session()->flash('thongbao','Thông tin tài khoản / mật khẩu không chính xác!');
            return redirect()->route('room.login');
        }
    }
    public function logout()
    {
        $this->guard()->logout();
        return redirect()->route('room.index');
    }

}
