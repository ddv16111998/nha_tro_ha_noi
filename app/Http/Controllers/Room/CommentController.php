<?php

namespace App\Http\Controllers\Room;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Repositories\CommentRepository;
use Carbon\Carbon;
class CommentController extends Controller
{
    private $repository;
    function __construct(CommentRepository $comment)
    {
        $this->repository = $comment;
    }
    public function save($id,$id_post,Request $request)
    {
        $post_id = $id_post;
        $comment = $request->comment;
        $user_id_comment = \Auth::guard('web')->user()->id;
        $data = [
            'post_id'=>$post_id,
            'comment'=>$comment,
            'user_comment_id'=>$user_id_comment
        ];
        return $this->repository->updateOrCreate($id,$data);
    }
    public function store($id_post,Request $request)
    {
        $data = $this->save($id = null,$id_post,$request);
        if($data)
        {
            $comment = $this->repository->getDataLast();
            $time = $comment->created_at;
            return redirect()->route('room.details',['id_post'=>$id_post]);
        }
    }
}
