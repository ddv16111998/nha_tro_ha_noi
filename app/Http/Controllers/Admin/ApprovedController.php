<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;
use App\Repositories\RoomRepository;

class ApprovedController extends Controller
{
    private $repository;
    private $stt = 1;
    function __construct(RoomRepository $room)
    {
        $this->repository = $room;
    }

    public function list(Request $request)
    {
        if($request->ajax())
        {
            $data = $this->repository->getDataRoomNeedApproved();
            return Datatables::of($data)
                ->addColumn('stt',function ($data){
                    return $this->stt++;
                })
                ->addColumn('user_id',function ($data){
                    if($data->getUser()->get())
                    {
                        return $data->getUser->name;
                    }
//
                })
                ->addColumn('action',function ($data){
                    $string = '
                      <a href="'.route('admin.approved.handle',$data->id).'" class="btn btn-xs btn-success"><i class="fa fa-check"></i> Duyệt</a>
                      <a href="" data-href="" class="btn btn-xs btn-danger btn-delete"><i class="fa fa-times"></i> Xóa</a>
                      <a href="" class="btn btn-xs btn-warning"><i class="fa fa-eye"></i> Chi tiết</a>
                      ';
                    return $string;
                })
                ->rawColumns(['stt','user_id','action'])
                ->toJson();
        }
        return view('admin.approved.index');
    }
    public function handle($id,Request $request)
    {
        $data = $this->repository->getDataById($id);
        $data->approve = 1;
        $data->save();
        if($data)
        {
            $request->session()->flash('thongbao', 'Bạn đã duyệt tin thành công!');
            return redirect()->route('admin.approved.list');
        }
        $request->session()->flash('thongbao', 'Bạn đã duyệt thất bại!');
        return redirect()->route('admin.approved.list ');
    }
}
