<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;
use App\Repositories\DistrictRepository;

class DistrictController extends Controller
{
    private $repository;
    function __construct(DistrictRepository $district)
    {
        $this->repository = $district;
    }

    public function index(Request $request)
    {
        if($request->ajax())
        {
            $data = $this->repository->getDataIndex();
            return Datatables::of($data)
                ->addColumn('action',function ($data){
                    $string = '
                      <a href="#" class="btn btn-xs btn-warning"><i class="fa fa-edit"></i>Sửa</a>
                      <a href="#" data-href="" class="btn btn-xs btn-danger btn-delete"><i class="fa fa-times"></i>Xóa</a>
                      ';
                    return $string;
                })
                ->rawColumns(['action'])
                ->toJson();
        }
        return view('admin.district.index');
    }
}
