<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;
use App\Repositories\CommuneRepository;

class CommuneController extends Controller
{
    private $repository;
    function __construct(CommuneRepository $commune)
    {
        $this->repository = $commune;
    }
    public function index(Request $request)
    {
        if($request->ajax())
        {
            $data = $this->repository->getDataIndex();
            return Datatables::of($data)
                ->editColumn('district_id',function ($data){
                    if($data->district())
                    {
                        return $data->district->name;
                    }
                    return '---';
                })
                ->addColumn('action',function ($data){
                    $string = '
                      <a href="#" class="btn btn-xs btn-warning"><i class="fa fa-edit"></i>Sửa</a>
                      <a href="#" data-href="" class="btn btn-xs btn-danger btn-delete"><i class="fa fa-times"></i>Xóa</a>
                      ';
                    return $string;
                })
                ->rawColumns(['action','district_id'])
                ->toJson();
        }
        return view('admin.commune.index');
    }

}
