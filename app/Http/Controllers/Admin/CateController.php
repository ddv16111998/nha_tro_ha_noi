<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;
use App\Repositories\CateRepository;
use App\Http\Requests\StoreAddCatePost;
class CateController extends Controller
{
    private $repository;

    function __construct(CateRepository $cate)
    {
        $this->repository = $cate;
    }
    public function index(Request $request)
    {
        if($request->ajax())
        {
            $data = $this->repository->getDataIndex();
            return Datatables::of($data)
                ->addColumn('action',function ($data){
                    $string = '
                      <a href="'.route('admin.cate.edit',$data->id).'" class="btn btn-xs btn-warning"><i class="fa fa-edit"></i>Sửa</a>
                      <a href="'.route('admin.cate.destroy',$data->id).'" data-href="" class="btn btn-xs btn-danger btn-delete"><i class="fa fa-times"></i>Xóa</a>
                      ';
                    return $string;
                })
                ->rawColumns(['action'])
                ->toJson();
        }
        return view('admin.cate.index');
    }
    public function create()
    {
        return view('admin.cate.create');
    }
    public function save(Request $request,$id)
    {
        $cate = $request->category;
        $slug = \Str::slug($cate);
        $data = [
            'name' => $cate,
            'slug' => $slug
        ];
        return $this->repository->updateOrCreate($data,$id);
    }
    public function store(StoreAddCatePost $request,$id=null)
    {
        $data = $this->save($request,$id);
        if($data)
        {
            $request->session()->flash('thongbao', 'Bạn đã tạo danh mục thành công!');
            return redirect()->route('admin.cate.index');
        }
        $request->session()->flash('thongbao', 'Bạn đã tạo danh mục thất bại!');
        return redirect()->route('admin.cate.index');
    }
    public function edit($id)
    {
        $cate = $this->repository->getDataById($id);
        return view('admin.cate.edit',['cate'=> $cate]);
    }
    public function update(StoreAddCatePost $request,$id)
    {
        $data = $this->save($request,$id);
        if($data)
        {
            $request->session()->flash('thongbao', 'Bạn đã sửa danh mục thành công!');
            return redirect()->route('admin.cate.index');
        }
        $request->session()->flash('thongbao', 'Bạn đã sửa danh mục thất bại!');
        return redirect()->route('admin.cate.index');
    }
    public function destroy($id,Request $request)
    {
        $data = $this->repository->destroy($id);
        if($data)
        {
            $request->session()->flash('thongbao', 'Bạn đã xóa danh mục thành công!');
            return redirect()->route('admin.cate.index');
        }
        $request->session()->flash('thongbao', 'Bạn đã xóa danh mục thất bại!');
        return redirect()->route('admin.cate.index');
    }



}
