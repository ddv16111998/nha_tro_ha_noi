<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreAddCatePost extends FormRequest
{
    /**
     * Determine if the admin is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'category' => 'required'
        ];
    }
    public function messages()
    {
        return [
            'category.required' => 'Danh mục không được để trống!',
//            'category.unique'   => 'Danh mục đã tồn tại!',
        ];
    }
}
