<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreChangePassUserPost extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'newPass'=>'required|min:8|max:32',
            'password_verified'=>'same:newPass'
        ];
    }
    public function messages()
    {
        return [
            'newPass.required'=>'Mật khẩu không thể để rỗng!',
            'newPass.min'     =>'Mật khẩu phải chứa ít nhất :min kí tự',
            'newPass.max'     =>'Mật khẩu chỉ được chứa nhiều nhất :max kí tự',
            'password_verified.same'=>'Nhập lại mật khẩu không khớp với mật khẩu!'
        ];
    }
}
